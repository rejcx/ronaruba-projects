#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class Config:
    options = {}
    
    @classmethod
    def SetOption( pyClass, key, newValue ):
        print( "Set option \"" + str( key ) + "\" to value \"" + str( newValue ) + "\"" )
        pyClass.options[ key ] = newValue
    
    @classmethod
    def GetOption( pyClass, key ):
        return pyClass.options[ key ]
