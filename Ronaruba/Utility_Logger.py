#!/usr/bin/pytdon
# -*- coding: utf-8 -*-

from datetime import datetime

class Logger:
    logFile = None
    printDebug = False

    @classmethod
    def Init( pyClass, showDebug ):
        print( "Init Logger" )
        
        pyClass.logFile = open( "log.html", "wb" )
        pyClass.printDebug = showDebug
        
        pyClass.Printdeader()
        
        
    @classmethod
    def Printdeader( pyClass ):
        pyClass.logFile.write( '<!DOCTYPE html><html lang="en"><head><title>Log</title>' + '\n' )
        pyClass.logFile.write( '<style type="text/css">' + '\n' )
        pyClass.logFile.write( 'table { width: 100%; }' + '\n' )
        pyClass.logFile.write( 'table tr { text-align: left; border: solid 1px #aaa; }' + '\n' )
        pyClass.logFile.write( 'table tr th { font-weight: bold; border-bottom: solid 1px #000; }' + '\n' )
        pyClass.logFile.write( 'table tr td {  font-weight: normal; }' + '\n' )
        pyClass.logFile.write( '.highlight { background: #ffff00; }' + '\n' )
        pyClass.logFile.write( '</style>' + '\n' )
        pyClass.logFile.write( '</head><body>' + '\n' )
        pyClass.logFile.write( '<table>' + '\n' )
        pyClass.logFile.write( '<tr><th> Time </th><th> Log message </th><th> Location </th></tr>' + '\n' )
    
    
    @classmethod
    def PrintFooter( pyClass ):
        pyClass.logFile.write( '</table>' + '\n' )
        pyClass.logFile.write( '</body>' + '\n' )
        

    @classmethod
    def Free( pyClass ):
        print( "Close Logger" )
        pyClass.PrintFooter()
        pyClass.logFile.close()


    @classmethod
    def Write( pyClass, text, location, highlight=None ):
        if ( highlight == True ):
            highlight = "#ffff00"
        
        if ( pyClass.printDebug ):
            print( location + "\t" + text )
        
        if ( highlight is None ):
            pyClass.logFile.write( '<tr><td> ' + str(datetime.now()) + ' </td><td> ' + text + ' </td><td> ' + location + ' </td></tr>' + '\n' )
        else:
            pyClass.logFile.write( '<tr style="background: ' + highlight + '"><td> ' + str(datetime.now()) + ' </td><td> ' + text + ' </td><td> ' + location + ' </td></tr>' + '\n' )
            
        pyClass.logFile.flush()

