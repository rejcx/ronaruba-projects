#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class Cursor( object ):
    def __init__( self, options ):
        self.image = None
        self.posRect = None
        self.frameRect = None
        self.playerIndex = 0
        self.moveSpeed = 10
        
        if ( options is not None ):
            self.Setup( options )
    
    def Setup( self, options ):
        if ( options["frameRect"] is not None ):
            self.frameRect = options["frameRect"]
        
        if ( options["posRect"] is not None ):
            self.posRect = options["posRect"]
            
        if ( options["image"] is not None ):
            self.image = options["image"]
            
    def MoveLeft( self ):
        self.posRect.x -= self.moveSpeed
            
    def MoveRight( self ):
        self.posRect.x += self.moveSpeed
            
    def MoveUp( self ):
        self.posRect.y -= self.moveSpeed
            
    def MoveDown( self ):
        self.posRect.y += self.moveSpeed
        
    def SetFrame( self, frameX, frameY ):
        self.frameRect.x = frameX
        self.frameRect.y = frameY
        
    def SetFrameX( self, frameX ):
        self.frameRect.x = frameX
    
    def SetFrameY( self, frameY ):
        self.frameRect.y = frameY
        
    def SetPosition( self, posX, posY ):     
        self.posRect.x = posX
        self.posRect.y = posY
        
    def GetPosition( self ):
        return self.posRect.x, self.posRect.y
        
    def GetX( self ):
        return self.posRect.x
        
    def GetY( self ):
        return self.posRect.y

    def Draw( self, windowSurface ):
        if ( self.image is not None ):
            windowSurface.blit( self.image, self.posRect, self.frameRect )
