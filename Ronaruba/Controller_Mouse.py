#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

class Mouse( object ):
    def __init__( self ):
        self.triggers = {
            "left-click" : False,
            "middle-click" : False,
            "right-click" : False,
            "scroll-up" : False,
            "scroll-down" : False,
            "move" : False
        }
        self.mouseX = 0
        self.mouseY = 0
        self.cooldown = 0
        self.image = None
        
    def SetImage( self, image ):
        self.image = image
        
    def IsPressed( self, buttonIndex ):
        return self.joystick.get_button( buttonIndex ) == True
        
    def IsLeftClick( self ):
        return self.triggers["left-click"], self.mouseX, self.mouseY
        
    def IsMiddleClick( self ):
        return self.triggers["middle-click"], self.mouseX, self.mouseY
        
    def IsRightClick( self ):
        return self.triggers["right-click"], self.mouseX, self.mouseY
        
    def IsMouseWheelUp( self ):
        return self.triggers["scroll-up"], self.mouseX, self.mouseY
        
    def IsMouseWheelDown( self ):
        return self.triggers["scroll-down"], self.mouseX, self.mouseY
        
    def GetMousePosition( self ):
        return self.mouseX, self.mouseY
        
    def SetMousePosition( self, x, y ):
        self.mouseX = x
        self.mouseY = y
        
    def MoveLeft( self, amount ):
        self.mouseX -= amount
        
    def MoveRight( self, amount ):
        self.mouseX += amount
        
    def MoveUp( self, amount ):
        self.mouseY -= amount
        
    def MoveDown( self, amount ):
        self.mouseY += amount

    # For button cooldowns (like in menus) where you don't want
    # something to get rapid-fire triggered.
    def GetCooldown( self ):
        return self.cooldown
    
    def SetCooldown( self, value ):
        self.cooldown = value
        
    def ResetTriggers( self ):
        self.triggers = {
            "left-click" : False,
            "middle-click" : False,
            "right-click" : False,
            "scroll-up" : False,
            "scroll-down" : False,
            "move" : False
        }        
        
    def Update( self, events ):
        self.ResetTriggers()
        
        print( "TOTAL EVENTS", len( events ) )
        
        for event in events:
            if ( event.type == MOUSEBUTTONDOWN ):                
                if      ( event.button == 1 ):   self.triggers["left-click"] = True
                if      ( event.button == 2 ):   self.triggers["middle-click"] = True
                if      ( event.button == 3 ):   self.triggers["right-click"] = True
                if      ( event.button == 4 ):   self.triggers["scroll-up"] = True
                if      ( event.button == 5 ):   self.triggers["scroll-down"] = True
                
            if ( event.type == MOUSEMOTION ):
                print( event.pos )
                self.mouseX, self.mouseY = pygame.mouse.get_pos()
        
        if ( self.cooldown > 0 ):
            self.cooldown -= 1        

    def Draw( self, windowSurface ):
        if ( self.image is not None ):
            windowSurface.blit( self.image, ( self.mouseX, self.mouseY ) )
