#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from State_Base import BaseState

from Controller_GamepadManager import GamepadManager
from Controller_Cursor import Cursor

from Widget_Label import Label
from Widget_Button import Button

class MainMenuState( BaseState ):
    def __init__( self ):
        self.stateName = "main_menu_state"
        self.textures = {}
        self.buttons = []
        self.labels = []
        self.images = []
        self.cursors = {}

    def Setup( self, screenWidth, screenHeight ):
        print( "Set up game" )
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""
        
        #self.background = pygame.image.load( "content/graphics/ui/menu-background.png" )
        self.textures["cursors"] = pygame.image.load( "content/graphics/ui/cursor-grid.png" )

        print( "Total gamepads: " + str( GamepadManager.GetTotalGamepads() ) )
        for i in range( GamepadManager.GetTotalGamepads() ):

            self.cursors[ "player" + str( i ) ] = Cursor( {
                "frameRect" : pygame.Rect( i*36, 0, 36, 49 ),
                "posRect" : pygame.Rect( i * (self.screenWidth/4) + 120, self.screenHeight/2, 36, 49 ),
                "image" : self.textures["cursors"]
            } )

        self.SetupMenu()

    def Update( self, events ):
        for event in events:
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos

        for i in range( GamepadManager.GetTotalGamepads() ):
            currentGamepad = GamepadManager.GetGamepad( i )
            currentCursor = self.cursors[ "player" + str( i ) ]

            # Moving cursor
            if ( currentGamepad.IsDirectionLeft( True ) ):
                currentCursor.MoveLeft()

            if ( currentGamepad.IsDirectionRight( True ) ):
                currentCursor.MoveRight()

            if ( currentGamepad.IsDirectionUp( True ) ):
                currentCursor.MoveUp()

            if ( currentGamepad.IsDirectionDown( True ) ):
                currentCursor.MoveDown()

            # Confirm button
            if ( currentGamepad.IsButtonPressed( currentGamepad.Button[ "B" ] ) ):
                # Check to see if it's on a button

                for button in self.buttons:
                    if ( button.IsClicked( currentCursor.GetX(), currentCursor.GetY() ) ):
                        action = button.GetAction()

                        if      ( action == "select-rose" ):    currentCursor.SetFrameY( 1 * 49 )
                        elif    ( action == "select-natalie" ): currentCursor.SetFrameY( 2 * 49 )
                        elif    ( action == "select-ruby" ):    currentCursor.SetFrameY( 3 * 49 )
                        elif    ( action == "select-basel" ):   currentCursor.SetFrameY( 4 * 49 )

    def Draw( self, windowSurface ):
        #windowSurface.blit( self.background, ( 0, 0 ) )

        self.DrawMenu( windowSurface )

        GamepadManager.Draw( windowSurface )

        for key, cursor in self.cursors.items():
            cursor.Draw( windowSurface )

    def Clear( self ):
        pass

    def SetupMenu( self ):
        self.backgroundColor = pygame.Color( 50, 255, 50 )
        self.textColor = pygame.Color( 105, 75, 0 )
        self.fntMain = pygame.font.Font( "content/fonts/Carlito-Bold.ttf", 20 )
        self.fntHeader = pygame.font.Font( "content/fonts/ThatNogoFontCasual.ttf", 50 )

        self.textures["avatar-basel"]      = pygame.image.load( "content/graphics/ui/avatar-basel.png" )
        self.textures["avatar-natalie"]    = pygame.image.load( "content/graphics/ui/avatar-natalie.png" )
        self.textures["avatar-rose"]       = pygame.image.load( "content/graphics/ui/avatar-rose.png" )
        self.textures["avatar-ruby"]       = pygame.image.load( "content/graphics/ui/avatar-ruby.png" )
        self.textures["button"]            = pygame.image.load( "content/graphics/ui/button-rect.png" )

        self.labels.append( Label( {
            "label_font" : self.fntHeader,
            "label_text" : "Who is playing?",
            "label_position" : ( 10, 10 ),
            "label_color" : self.textColor
        } ) )

        x = 65
        y = self.screenHeight / 3
        inc = self.screenWidth / 5

        self.buttons.append( Button( {
            "button_texture" : self.textures[ "avatar-rose" ],
            "button_position" : ( x + (0 * inc), y ),
            "button_action" : "select-rose",
            "button_type" : "confirm"
            } ) )

        self.buttons.append( Button( {
            "button_texture" : self.textures[ "avatar-natalie" ],
            "button_position" : ( x + (1 * inc), y ),
            "button_action" : "select-natalie",
            "button_type" : "confirm"
            } ) )

        self.buttons.append( Button( {
            "button_texture" : self.textures[ "avatar-ruby" ],
            "button_position" : ( x + (2 * inc), y ),
            "button_action" : "select-ruby",
            "button_type" : "confirm"
            } ) )

        self.buttons.append( Button( {
            "button_texture" : self.textures[ "avatar-basel" ],
            "button_position" : ( x + (3 * inc), y ),
            "button_action" : "select-basel",
            "button_type" : "confirm"
            } ) )

        self.buttons.append( Button( {
            "button_texture" : self.textures[ "button" ],
            "button_position" : ( self.screenWidth - 100, 10 ),
            "button_action" : "confirm-players",
            "button_type" : "confirm",
            "label_text" : "Start!",
            "label_position" : ( 30, 10 ),
            "label_font" : self.fntMain,
            "label_color" : pygame.Color( 210, 220, 100 )
            } ) )

    def DrawMenu( self, windowSurface ):
        for button in self.buttons:
            button.Draw( windowSurface )

        for label in self.labels:
            label.Draw( windowSurface )
