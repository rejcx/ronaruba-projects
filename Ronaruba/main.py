#!/usr/bin/python
# -*- coding: utf-8 -*-

from PygameProgram import PygameProgram
from Utility_Config import Config

from State_MainMenu import MainMenuState

import pygame, sys
from pygame.locals import *

game = PygameProgram()
game.Setup( "Ronaruba Game Collection", True, 1680, 1050 )
screenWidth, screenHeight = game.GetScreenDimensions()

list_modes = pygame.display.list_modes()
print( list_modes )

Config.SetOption( "player-info", [
    {
        "name" : "Basel",
        "color" : pygame.Color( 158, 255, 86 )
    },
    {
        "name" : "Natalie",
        "color" : pygame.Color( 105, 186, 255 )
    },
    {
        "name" : "Rose",
        "color" : pygame.Color( 255, 86, 189 )
    },
    {
        "name" : "Ruby",
        "color" : pygame.Color( 0, 216, 188 )
    }
] )

state = MainMenuState()
state.Setup( screenWidth, screenHeight )

while ( game.IsDone() is False ):
    game.CycleBegin()

    for event in game.GetEvents():
        if event.type == KEYDOWN:
            if event.key == K_F4:
                game.Done()

    state.Update( game.GetEvents() )
    state.Draw( game.GetWindow() )

    game.CycleEnd()

game.Teardown()
