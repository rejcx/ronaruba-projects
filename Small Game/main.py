#!/usr/bin/python
# -*- coding: utf-8 -*-

from PygameProgram import PygameProgram
from Controller_GamepadManager import GamepadManager

import pygame, sys
from pygame.locals import *

game = PygameProgram()
game.Setup( "Small Game", True, 1680, 1050 )
screenWidth, screenHeight = game.GetScreenDimensions()

list_modes = pygame.display.list_modes()
print( list_modes )

GamepadManager.Setup()

imgGrass = pygame.image.load( "content/graphics/grass.png" )
imgGirl = pygame.image.load( "content/graphics/girl.png" )
girlX = screenWidth/2
girlY = screenHeight/2

while ( game.IsDone() is False ):
    game.CycleBegin()
    
    # Get input
    for event in pygame.event.get():
        if event.type == QUIT:
                game.Done()
                
        if event.type == MOUSEMOTION:
            mouseX, mouseY = event.pos
            
        if event.type == KEYDOWN:
            if event.key == K_F4:
                game.Done()
    
    GamepadManager.Update()
    
    for gamepad in GamepadManager.GetAllGamepads():
		if ( gamepad.IsDirectionLeft() ):
			girlX -= 5
		elif ( gamepad.IsDirectionRight() ):
			girlX += 5
		
		if ( gamepad.IsDirectionUp() ):
			girlY -= 5
		elif ( gamepad.IsDirectionDown() ):
			girlY += 5
                
    # Drawing
#    #for y in range( screenHeight / 32 + 1 ):
#		for x in range( screenWidth / 32 + 1 ):
#			game.GetWindow().blit( imgGrass, ( x * 32, y * 32 ) )
    
    game.GetWindow().blit( imgGirl, ( girlX, girlY ) )

    game.CycleEnd()

game.Teardown()
